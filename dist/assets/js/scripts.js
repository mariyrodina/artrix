$(document).ready(function() {
    try{
        $('#fullpage').onepage_scroll({
            sectionContainer: "section",
            responsiveFallback: 0,
            loop: false,
         });
    }catch(e){
        //console.log(e);
    }
     openMenu();
     initCarousel();
     showMap();
     focusInput();
     dowloadFile();
     showForm()
}); 

function showMap(){
    $('#map').mouseenter(function(){
        $(this).css('background','url(assets/images/25.gif) no-repeat 50%');
        initMap();
    });
}
function initMap() {
var map;
var uluru = {lat: 60.018969, lng: 30.332519};
map = new google.maps.Map(document.getElementById('map'), {
    center: uluru,
    zoom: 10,
    disableDefaultUI: true,
    zoomControl: true,

});
// Create a new StyledMapType object, passing it an array of styles,
// and the name to be displayed on the map type control.
var styledMapType = new google.maps.StyledMapType(
    [
        {elementType: 'geometry', stylers: [{color: '#212121'}]},
        {elementType: 'labels.text.fill', stylers: [{color: '#bdbdbd'}]},
        {elementType: 'labels.text.stroke', stylers: [{color: '#000'}]},
        {
        featureType: 'administrative',
        elementType: 'geometry.stroke',
        stylers: [{color: '#212121'}]
        },
        {
        featureType: 'administrative.land_parcel',
        elementType: 'geometry.stroke',
        stylers: [{color: '#212121'}]
        },
        {
        featureType: 'administrative.land_parcel',
        elementType: 'labels.text.fill',
        stylers: [{color: '#212121'}]
        },
        {
        featureType: 'landscape.natural',
        elementType: 'geometry',
        stylers: [{color: '#212121'}]
        },
        {
        featureType: 'poi',
        elementType: 'geometry',
        stylers: [{color: '#212121'}]
        },
        {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{color: '#212121'}]
        },
        {
        featureType: 'poi.park',
        elementType: 'geometry.fill',
        stylers: [{color: '#6c6666'}]
        },
        {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{color: '#6c6666'}]
        },
        {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{color: '#212121'}]
        },
        {
        featureType: 'road.arterial',
        elementType: 'geometry',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'road.highway.controlled_access',
        elementType: 'geometry',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'road.highway.controlled_access',
        elementType: 'geometry.stroke',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'road.local',
        elementType: 'labels.text.fill',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'transit.line',
        elementType: 'geometry',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'transit.line',
        elementType: 'labels.text.fill',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'transit.line',
        elementType: 'labels.text.stroke',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'transit.station',
        elementType: 'geometry',
        stylers: [{color: '#4e4e4e'}]
        },
        {
        featureType: 'water',
        elementType: 'geometry.fill',
        stylers: [{color: '#000000'}]
        },
        {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [{color: '#000000'}]
        }
    ],
    {name: 'Styled Map'});
    var marker = new google.maps.Marker({position: uluru, map: map, icon:"assets/images/pin.png"});
    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
    $('#map').unbind('mouseenter');
}

function openMenu(){

    if($(window).width() >= 768){
        $('#burger').click(function(){
            $('#menu').slideToggle();
        });
        $('#menu_close').click(function(){
            $('#menu').slideToggle();
        });
    }
    else{
        $('#burger').click(function(){
            $('#menu').addClass('open');
        });
        $('#menu_close').click(function(){
            $('#menu').removeClass('open');
        });
    }

    $(window).resize(function(){

        if($(window).width() >= 768){
            location.reload();
        }
        else{
            $('#burger').unbind('click');
            $('#menu_close').unbind('click');

            $('#burger').click(function(){
                $('#menu').addClass('open');
            });
            $('#menu_close').click(function(){
                $('#menu').removeClass('open');
            });
        }

    });

}

function initCarousel(){
    $('#solutions').owlCarousel({
        items:1,
        autoPlay : true,
        
    });
    var owl = $('#solutions');
    owl.owlCarousel();
    //Переключение слайда по клику на ссылки направлений
    $('.section__nav').click(function(e){
        let pos = $(e.target).attr('data-carousel');
        $('.section__nav').find('[data-carousel]').removeClass('active');
        $('.section__nav').find('[data-carousel = ' + pos + ']').addClass('active');
        if(pos){
            owl.trigger('to.owl.carousel', [pos, 1000]);
        }
    });

}

function focusInput(){
    $('.form__input').find('input').focus(function(){
        $(this).parents('.form__row').addClass('focus');
    });
    $('.form__input').find('input').blur(function(){
        console.log($(this).val());
        if($(this).val() == ""){
            $(this).parents('.form__row').removeClass('focus');
        }
    });

    $('.form__input').find('textarea').focus(function(){
        $(this).parents('.form__row').addClass('focus');
    });
    $('.form__input').find('textarea').blur(function(){
        console.log($(this).val());
        if($(this).val() == ""){
            $(this).parents('.form__row').removeClass('focus');
        }
    });
}

function dowloadFile(){
    $('.form__file input').change(function(){
        let val = $(this).val();
        val = val.split('\\');
        let name_file = val[val.length-1];
        if(name_file != ''){
            $(this).parents('.form__file').find('span').text(name_file); 
        }
    });
}

function showForm(){
    $('.start').click(function(e){
        e.preventDefault;
        var scrollTop = $(document).scrollTop();
        $('.popup-form').css('top',scrollTop);
        $('.popup-form').fadeIn();
        $('.popup-form__close').click(function(){
            $('.popup-form').fadeOut();
        });
    });
}

